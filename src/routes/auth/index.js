import { createAppContainer, createStackNavigator } from 'react-navigation';

// screens
import Login from '../../screens/auth/login';

const authStackNavigator = createStackNavigator(
  {
    Login
  },
  {
    initialRouteName: 'Login',
    defaultNavigationOptions: {
      header: null
    }
  }
);

const authNavigator = createAppContainer(authStackNavigator);

export default authNavigator;
