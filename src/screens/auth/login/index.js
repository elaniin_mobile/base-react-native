
import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';

//customs
import styles from './styles';

import en from '../../../locale/en';

//actions
import { INIT_SESSION } from '../../../actions/auth';

class Login extends Component {
  onPressLogin = () => {
    const { dispatch } = this.props;

    dispatch({
      type: INIT_SESSION,
      payload: {
        authorize: true
      }
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>
          {en.login_screen_title}
        </Text>
        <TouchableOpacity
          onPress={() => this.onPressLogin()}
        >
          <Text>
            {en.login_button_text}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default connect()(Login);