## [ADD] 2019-05-18 09:08
  * Initialize app
  * Add firebase in ios and android

## [ADD] 2019-05-24 16:58
  * Add splash screen in android and ios

## [ADD] 2019-05-24 17:13
  * Add redux, native base and react native elements

## [ADD] 2019-05-24 17:30
  * Add navigation

## [ADD] 2019-05-26 23:50
  * Add firebase storage in android and ios
  * Add utils
  * add moment and axios

## [ADD] 2019-05-27 00:48
  * Add install instruction

## [FIX] 2019-05-28 11:42
  * Fix error when signed apk release

## [ADD] 2019-06-05 14:44
  * Add code for export bundle in android